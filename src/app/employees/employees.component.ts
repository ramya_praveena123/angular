import { Component,OnInit } from '@angular/core';

@Component({
  selector: 'app-show-employees',
  templateUrl: './Employees.component.html',
  styleUrl: './employees.component.css'
})
export class ShowEmployeesComponent implements OnInit {
  employees:any[]=[
    {empId:1,empName:'Ramya',salary:10770.00,gender:'FeMale',doj:'2021-18-35',country:'INDIA',emailId:'ramya@gmail.com',password:'ramya'},
    {empId:2,empName:'prashanth',salary:8860.00,gender:'Male',doj:'2021-13-11',country:'INDIA',emailId:'prashanth@gmail.com',password:'prassu'},
    {empId:3,empName:'sravya',salary:98768.00,gender:'FeMale',doj:'2018-07-25',country:'INDIA',emailId:'sravya@gmail.com',password:'sravya'},
    {empId:4,empName:'swetha',salary:872481.00,gender:'Female',doj:'2020-12-01',country:'INDIA',emailId:'swetha@gmail.com',password:'swetha'},
  ]
 constructor(){

 }
 ngOnInit(){
}
submit(){
  console.log(this.employees)
}

}