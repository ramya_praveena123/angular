import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-showempbyid',
  templateUrl: './showempbyid.component.html',
  styleUrl: './showempbyid.component.css'
})
export class ShowempbyidComponent implements OnInit {

  empId: any;
  emp: any;
  employees: any;

  constructor() {
    this.employees = [
      {empId:101, empName:'Ramya',  salary:1212.12, gender:'FeMale',   doj:'05-25-2018', country:"IND", emailId:'ramya@gmail.com',  password:'123'},
      {empId:102, empName:'Rohith',   salary:2323.23, gender:'Male',   doj:'06-26-2017', country:"USA", emailId:'rohith@gmail.com',   password:'123'},
      {empId:103, empName:'swetha',  salary:3434.34, gender:'Female', doj:'07-27-2016', country:"CHI", emailId:'swetha@gmail.com',  password:'123'},
      {empId:104, empName:'prashanth',  salary:4545.45, gender:'Male',   doj:'08-28-2015', country:"JAP", emailId:'prashanth@gmail.com',  password:'123'},
      {empId:105, empName:'sravya', salary:5656.56, gender:'Male',   doj:'09-29-2014', country:"UK",  emailId:'sravya@gmail.com', password:'123'}
    ];
  }

  ngOnInit() {
  }

  getEmployee() {

    this.emp = null;

    this.employees.forEach((employee: any) => {
      if (employee.empId == this.empId) {
        this.emp = employee;
      }
    });
  }

}
