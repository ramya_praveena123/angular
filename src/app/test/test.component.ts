import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent implements OnInit {

    id: number;
    name: string;
    avg: number;
    address: any;
    hobbies: any;
  constructor(){
    alert("constructor invoked");

    this.id=101;
    this.name='Ramya Don';
    this.avg=45.65;

    this.address={
      StreetNo:101,
      City:'Hyderabad',
      State:'Telangana',
      pincode:506112
    };
    this.hobbies = ['mudder','kidnap','cain snaching','shooting ']

  }
 ngOnInit(){
  alert("ngOnInit invoked");

 }
}